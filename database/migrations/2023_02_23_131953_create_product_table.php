<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->comment('');
            $table->integer('id_pro', true);
            $table->string('name', 45)->nullable();
            $table->string('tipo', 45)->nullable();
            $table->integer('cantidad')->nullable();
            $table->double('val_uni')->nullable();
            $table->string('productcol', 45)->nullable();
            $table->string('descripcion', 45)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
};
