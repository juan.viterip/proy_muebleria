<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suplier', function (Blueprint $table) {
            $table->comment('');
            $table->integer('id_sup', true);
            $table->string('name', 45)->nullable();
            $table->string('addres', 45)->nullable();
            $table->string('company', 45)->nullable();
            $table->string('mail', 45)->nullable();
            $table->string('celphone', 45)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('suplier');
    }
};
