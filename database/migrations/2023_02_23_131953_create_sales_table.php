<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->comment('');
            $table->integer('id_sales', true);
            $table->integer('id_cus')->index('id_cus_idx');
            $table->integer('id_pro')->index('id_pro_idx');
            $table->integer('can_ven')->nullable();
            $table->double('val_ven')->nullable();
            $table->string('fecha', 45)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
};
