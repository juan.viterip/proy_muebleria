@extends('layouts.app')

@section('content')
    <div class="container">
        <br />
        <h1 align="center">REPORTE DE CLIENTES</h1>
        <div>
            <a class="btn btn-success" href="{{ route('clientes.create') }}">Crear</a>
        </div>
        <br />
        @if (Session::get('success'))
            <div class="alert alert-success">
                <p>{{ Session::get('success') }}</p>
            </div>
        @endif
        <table class="table">
            <tr>
                <th>Id</th>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Direccion</th>
                <th>Telefono</th>
                <th>Mail</th>
            </tr>
            @foreach ($cliente as $cliente )
            <tr>
                <td>{{ $cliente->id }}</td>
                <td>{{ $cliente->nombre }}</td>
                <td>{{ $cliente->apellido }}</td>
                <td>{{ $cliente->direccion }}</td>
                <td>{{ $cliente->telefono }}</td>
                <td>{{ $cliente->mail }}</td>
                <td><a class="btn btn-primary" href="{{ route('clientes.edit',$cliente) }}">
                    <i class="fa-solid fa-pen-to-square"></i>
                </a>
                <form action="{{ route('clientes.destroy',$cliente->id) }}" method="POST">
                    @csrf
                    @method('DELETE')
                    <button type="submit" class="btn btn-primary"><i class="fas fa-trash-alt"></i></button>
                </form>
                </td>
            </tr>    
                
            @endforeach

@endsection