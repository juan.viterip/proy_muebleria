@extends('layouts.app')
@section('content')
    <div class="container">
        <div align="center">
            <h2>Agregar Nuevo Cliente</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('clientes.index') }}"> Regresar</a>
        </div>

        <form action="{{ route('clientes.store') }}" method="POST">
            @csrf

            <div class="row">
                <div>
                    <div class="form-group">
                        <strong>Nombre:</strong>
                        <input type="string" name="nombre" class="form-control"
                            placeholder="Ingrese el nombre">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Apellido:</strong>
                        <input type="string" name="apellido" class="form-control"
                            placeholder="Ingrese el apellidos">
                    </div>
                </div>
                
                <div>
                    <div class="form-group">
                        <strong>Direccion:</strong>
                        <input type="string" name="direccion" class="form-control"
                            placeholder="Ingrese la direccion">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Telefono:</strong>
                        <input type="string" name="telefono" class="form-control"
                            placeholder="Ingrese el telefono">
                    </div>
                </div>

                <div>
                    <div class="form-group">
                        <strong>Mail:</strong>
                        <input type="string" name="mail" class="form-control"
                            placeholder="Ingrese el mail">
                    </div>
                </div>

                <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                    <button type="submit" class="btn btn-primary">Guardar</button>
                </div>
            </div>
        </form>
    </div>
@endsection