@extends('layouts.app')
@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Editar Cliente</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('clientes.index') }}"> Regresar</a>
            </div>
        </div>
    </div>
    <form action="{{ route('clientes.update',$cliente->id) }}" method="POST">
        @csrf
        @method('PUT')
         <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Nombre:</strong>
                    <input type="string" name="nombre" class="form-control" value="{{ $cliente->nombre }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Apellido:</strong>
                    <input type="string" name="apellido" class="form-control" value="{{ $cliente->apellido }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Direccion:</strong>
                    <input type="string" name="direccion" class="form-control" value="{{ $cliente->direccion }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Telefono:</strong>
                    <input type="string" name="telefono" class="form-control" value="{{ $cliente->telefono }}">
                </div>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8" style="margin: 0 auto; float: none;">
                <div class="form-group">
                    <strong>Mail:</strong>
                    <input type="string" name="mail" class="form-control" value="{{ $cliente->mail }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
              <button type="submit" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </form>
@endsection